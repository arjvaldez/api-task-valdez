//
//  DetailViewController.swift
//  API Task
//
//  Created by Arjay on 9/18/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var newsDetailTitle: UILabel!
    @IBOutlet weak var newsDetailAuthor: UILabel!
    @IBOutlet weak var newsDetailText: UILabel!
    @IBOutlet weak var newsDetailImage: UIImageView!

    var newsfeedDetail: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let detail = newsfeedDetail else { return }
        newsDetailTitle.numberOfLines = 0
        newsDetailAuthor.numberOfLines = 0
        newsDetailText.numberOfLines = 0
        newsDetailText.text = "\(detail.description ?? "")\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae vestibulum mi. Phasellus eget urna et dolor ullamcorper placerat in a massa. Duis mattis tellus enim, a euismod nunc sodales mattis. Mauris varius purus at leo accumsan, in luctus libero faucibus. Phasellus non tempor metus, sit amet aliquet metus. Vestibulum mi diam, tempor id quam sed, ultricies laoreet quam. Nam rutrum, turpis nec scelerisque dapibus, sem neque congue orci, in posuere libero turpis sed diam. Fusce convallis tincidunt lacus, at fringilla leo gravida vel. Curabitur varius, tortor vel consectetur dictum, sem ante tempus magna, a pharetra leo lorem id arcu. Aliquam condimentum erat vel tempor pellentesque. Vivamus sed nisl in sapien lacinia convallis non id purus.\n\nPellentesque leo augue, dictum vel mauris eget, tempor viverra tellus. Pellentesque maximus iaculis leo, nec fermentum orci pulvinar vitae. Duis finibus viverra ullamcorper. Cras consequat sit amet dolor quis placerat. Morbi sed fermentum eros, sit amet ultricies elit. Proin venenatis nisl id tortor facilisis interdum. Maecenas nec urna a ipsum malesuada varius non at arcu. In ac blandit metus, non luctus velit. Vivamus sed dignissim est, ac iaculis nunc. Nam vulputate felis nec ex bibendum tempor. Morbi a nibh nec turpis lacinia interdum. Morbi maximus, eros vitae pretium tristique, arcu risus interdum mauris, non malesuada augue neque sed mi. Vestibulum eget tincidunt mauris, at iaculis quam."
        newsDetailTitle.text = detail.title
        newsDetailAuthor.text = "by \(detail.author ?? "")"
        let imageURL = detail.urlToImage
        if imageURL == nil{
            newsDetailImage.image = UIImage(named: "placeholder")
        }else{
             newsDetailImage.sd_setImage(with: URL(string: imageURL!))
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
