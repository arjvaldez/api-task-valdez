//
//  ViewController.swift
//  API Task
//
//  Created by Arjay on 9/15/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!

    let placeHolderImage = UIImage(named: "placeholder")
    var newsFeed = [Article]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
                self.navigationItem.title = "Latest News"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(didRefresh), for: .valueChanged)
        tableView.estimatedRowHeight = 30.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        refreshFeed()

    }
    
    func refreshFeed(){
        newsFeed.removeAll()
        let apiRequest = ApiRequest()
        apiRequest.getNews { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let news):
                self?.newsFeed = news
            }
        }
    }
    
    @objc func didRefresh(){
        refreshFeed()
    }
    
}

extension ViewController: UITableViewDataSource{
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleCell

        let news = self.newsFeed[indexPath.row]
        let imageURL:String? = news.urlToImage

        cell.newsLabel.numberOfLines = 0
        cell.newsLabel.text = news.title
        cell.newsDescription.numberOfLines = 2
        cell.newsDescription.text = news.description
        
        if imageURL == nil{
            cell.newsImage.image = placeHolderImage
        }else{
             cell.newsImage.sd_setImage(with: URL(string: imageURL!))
        }
        
        
        return cell
    }
//      Sir Israel's
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "segueToDetailVC" {
//            if let destinationVC = segue.destination as? DetailViewController {
//                if let indexPath = sender as? IndexPath {
//                    destinationVC.newsfeedDetail = newsFeed[indexPath.row]
//                }
//            }
//        }
//    }
}

extension ViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            tableView.beginUpdates()
            newsFeed.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
            tableView.endUpdates()
            
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailController = storyboard?.instantiateViewController(withIdentifier: "detail") as? DetailViewController{
            detailController.newsfeedDetail = newsFeed[indexPath.row]
            navigationController?.pushViewController(detailController, animated: true)
        }
        //Sir Israel's
        //performSegue(withIdentifier: "segueToDetailVC", sender: indexPath)
    }
    
}
