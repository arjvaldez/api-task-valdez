//
//  ArticleCell.swift
//  API Task
//
//  Created by Arjay on 9/16/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {


    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


}
